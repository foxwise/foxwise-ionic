import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform } from '@ionic/angular';
import { Profile } from './objects/Profile';
import { User } from './objects/user';
import { LoginService } from './services/login.service';
import { UtilService } from './services/util.service';
import { TranslateService } from './utils/translate/translate.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  public userLogged: User;
  public links: Array<any> = [];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private navController: NavController,
    private router: Router,
    private loginService: LoginService,
    private utilService: UtilService,
    private translate: TranslateService
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    await this.platform.ready()
    this.statusBar.styleDefault();
    this.splashScreen.hide();
    // this.utilService.setColorTheme("blue");
  }

  public async routerActived(page: any) {
    let currentRouter = "";
    if (page) {
      currentRouter = this.router.url;
      console.log(currentRouter);
      this.setSelectedRouter(this.links, currentRouter);
    }
    // if (!this.userLogged && currentRouter && currentRouter != "/login") {
    //   await this.utilService.loading();
    //   this.loginService.getAuth().subscribe( async data => {
    //     await this.setUser(data[ObjectMSG.OBJ]);
    //   }, async error => {
    //     await this.utilService.showMesseges(error);
    //   }, async () => {
    //     await this.utilService.dismissLoading();
    //   })
    // }
  }

  public async  setUser(user: User) {
    this.links = await this.createLinks(user.profiles);
    this.userLogged = user;
  }

  public async createLinks(profiles: Profile[]): Promise<Array<any>> {
    let links = [];
    if (profiles) {
      profiles.forEach(profile => {
        if (profile) {
          let actions = profile["actions"];
          if (actions) {
            actions.forEach(action => {
              if (action["group"]) {
                let group = links.find((l) => l["group"] == action["group"]);
                if (group) {
                  group["links"].push({ "name": action["menuLabel"], "router": action["router"], "icon": action["icon"], "selected": action["selected"] })
                } else {
                  links.push({ "group": action["group"], "links": [{ "name": action["menuLabel"], "router": action["router"], "icon": action["icon"], "selected": action["selected"] }] });
                }
              } else if (action["router"]) {
                links.push({ "name": action["menuLabel"], "router": action["router"], "icon": action["icon"], "selected": action["selected"] });
              }
            });
          }
        }
      })
    }
    return links;
  }

  setLinksTemp(user) {
    user["profiles"] = [
      {
        "actions": [
          { "menuLabel": "Dashboard", "router": "/login", "icon": "dashboard", "selected": true },
          { "menuLabel": "Relatório Cupons", "router": "/home", "icon": "list-box", "group": "Relatório" },
          { "menuLabel": "Relatório Pontos", "router": "/home", "icon": "list", "group": "Relatório" },
          { "menuLabel": "Sair", "router": "/login", "icon": "exit", }
        ]
      }];
  }

  public setSelectedRouter(list: Array<any> = [], currentRouter: string) {
    list.forEach((link) => {
      if (link["links"]) {
        this.setSelectedRouter(link["links"], currentRouter);
      } else if (link["router"]) {
        if (link["router"] == currentRouter) {
          link["selected"] = true;
        } else {
          link["selected"] = false;
        }
      }
    });
  }
}
