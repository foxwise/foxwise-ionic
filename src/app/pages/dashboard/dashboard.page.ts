import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UtilService } from 'src/app/services/util.service';
import Validation from 'src/app/utils/Validation';

@Component({
  selector: 'm-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  //--------------------- Input
  email: FormControl = new FormControl("", [
    this.utilService.validatorFunc(Validation.isEmpty, "Email é obrigatório"),
    this.utilService.validatorFunc(Validation.isEmailValid, "Email inválido")
  ], [//Async
    this.utilService.validatorAsyncAlreadyExist("get", "/v1/api/user/email/", "Email já está sendo utilizado!")
  ])

  //--------------------- Radio
  radio: FormControl = new FormControl("", [
    this.utilService.validatorFunc(Validation.isEmpty, "Por favor, selecione uma opção!")
  ])

  //--------------------- Checkbox
  checkbox: FormControl = new FormControl("", [
    this.utilService.validatorFunc(Validation.isEmpty, "Por favor, marque a opção!")
  ])

  //-------------------- TextArea
  textarea: FormControl = new FormControl("", [
    this.utilService.validatorFunc(Validation.isEmpty, "É necessário preencher a informação!")
  ])

  //--------------------- select
  //--------------------- Table
  dataTable = [
    {position: 1, code: "2982829", name: 'Smartband Watch Slim 4', input: new Date(2017, 2, 20), weight: 39.329, mark: "Xiomi", value: 232.50},
    {position: 2, code: "2982222", name: 'Galaxy S8 Plus', input: new Date(2018, 6, 1), weight: 75.352, mark: "Samsung", value: 3850.00},
    {position: 3, code: "2982829", name: 'Smartband Watch Slim 4', input: new Date(2017, 2, 20), weight: 39.329, mark: "Xiomi", value: 232.50},
    {position: 4, code: "2982222", name: 'Galaxy S8 Plus', input: new Date(2018, 6, 1), weight: 75.352, mark: "Samsung", value: 3850.00},
    {position: 5, code: "2982829", name: 'Smartband Watch Slim 4', input: new Date(2017, 2, 20), weight: 39.329, mark: "Xiomi", value: 232.50},
    {position: 6, code: "2982222", name: 'Galaxy S8 Plus', input: new Date(2018, 6, 1), weight: 75.352, mark: "Samsung", value: 3850.00},
    {position: 7, code: "2982829", name: 'Smartband Watch Slim 4', input: new Date(2017, 2, 20), weight: 39.329, mark: "Xiomi", value: 232.50},
    {position: 8, code: "2982222", name: 'Galaxy S8 Plus', input: new Date(2018, 6, 1), weight: 75.352, mark: "Samsung", value: 3850.00},
    {position: 9, code: "2982829", name: 'Smartband Watch Slim 4', input: new Date(2017, 2, 20), weight: 39.329, mark: "Xiomi", value: 232.50},
    {position: 10, code: "2982222", name: 'Galaxy S8 Plus', input: new Date(2018, 6, 1), weight: 75.352, mark: "Samsung", value: 3850.00},
    {position: 11, code: "3323232", name: 'Iphone XS Rose', input: new Date(2019, 12, 15), weight: 89.329, mark: "Apple", value: 6580.99},
  ];
  showHeaderSearch = false;
  constructor(
    private utilService: UtilService
    ) { }
  //--------------------- Drag and Drop

  cityList = [
    {id: 1, name: "Abreu e Lima"},
    {id: 2, name: "Piedade"},
    {id: 3, name: "Recife"},
    {id: 4, name: "Serra Talhada"},
    {id: 5, name: "Zumbi dos Palmares"},
    {id: 6, name: "Engenho Maranguape Velho"},
    {id: 7, name: "Paulista"},
    {id: 8, name: "Goiana"},
    {id: 9, name: "Candeias"},
    {id: 10, name: "Afagados"},
    {id: 11, name: "Prazeres"},
    {id: 12, name: "Chã de Cruz"},
  ];

  // ------------------------ Tree

  treeData = {
    "Cadastro": {
      'Comercial': null,
      'Desconto': null,
      'Grupo de Desconto': null,
      'Planos de Pagamento': null,
      'Alteração de Preços': null,
      "Controle Acesso": {
        "Usuário": null,
        "Perfil de Usuário": null,
        "Gestão de Acesso": null
      },
      "Tributos": null,
    },
    "Consultas Gerenciais": [
      "Preço Vigente",
      "Comparativo MID x P2K",
      "Tira-Teima - Código do Usuário",
      "Planejamento de Vendas",
      "Relatório COMP",
      "Pedidos por Faixa Horária",
      "Performance do Operador",
    ],
    "Suporte a Componentes": [
      "Backup/Restore de Base",
      "Carga de Base EP/SP",
      "Consulta de Componentes",
      "Solicitação de Arquivos",
      "Monitoração de Arquivos",
      "Comunicação PDV",
      "Prioridade Carga Base",
      "Tela Suporte"
    ],
    "Posto de Etiquetagem": [
      'Etiquetas de Preço',
      'Etiquetas de de Inventário',
      'Etiquetas Fora de Estoque',
    ]
  };



  ngOnInit() {
  }
}