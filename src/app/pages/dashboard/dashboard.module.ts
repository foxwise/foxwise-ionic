import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomComponentsModule } from 'src/app/custom-components/custom-components.module';
import { DashboardPage } from './dashboard.page';



const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CustomComponentsModule
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
