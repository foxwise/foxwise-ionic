import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomComponentsModule } from 'src/app/custom-components/custom-components.module';
import { LoginPage } from './login.page';



const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CustomComponentsModule
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
