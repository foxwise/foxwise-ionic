import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';
import { User } from 'src/app/objects/user';
import { LoginService } from 'src/app/services/login.service';
import { UtilService } from 'src/app/services/util.service';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { async } from '@angular/core/testing';
import { ObjectMSG } from 'src/app/enums/ObjectMSG';
import { SecretToken, Routers } from 'src/app/utils/values';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'm-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private user: User = {login: "mumps", password:"123456"};
  constructor(
    private app: AppComponent,
    private platform: Platform,
    private loginService: LoginService,
    private navController: NavController,
    private utilService: UtilService,
    private translate: TranslateService,
    public menuController: MenuController
    ) { }
    
    async ngOnInit() {
      this.menuController.enable(false);
      await this.platform.ready();
      await this.utilService.removeStorage(SecretToken.TOKEN);
      this.app.userLogged=  undefined;
      this.app.links = [];
  }
  
  async logout(){
    
  }
  
  async login(){
    await this.utilService.loading();
    this.loginService.login(this.user).subscribe(async data => {
      console.log(`error: ${data}`);
      await this.app.setUser(data[ObjectMSG.OBJ]);
      await this.utilService.setStorage(SecretToken.TOKEN, data[ObjectMSG.ACCESS_TOKEN]);
      await this.menuController.enable(false);
      await this.utilService.showMesseges(data);
      await this.navController.navigateRoot(Routers.HOME);
    }, 
    async error => {
      console.log(`error: ${error}`);
      await this.utilService.showMesseges(error);
    }, async () => {
      await this.utilService.dismissLoading();
    })
  }

}
