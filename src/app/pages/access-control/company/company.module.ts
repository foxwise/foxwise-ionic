import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomComponentsModule } from 'src/app/custom-components/custom-components.module';
import { CompanyPage } from './company.page';



const routes: Routes = [
  {
    path: '',
    component: CompanyPage,
    // canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CustomComponentsModule,
  ],
  declarations: [CompanyPage]
})
export class CompanyPageModule {}
