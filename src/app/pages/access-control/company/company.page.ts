import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CompanyService } from 'src/app/services/company.service';
import { UtilService } from 'src/app/services/util.service';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import Validation from 'src/app/utils/Validation';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'm-company',
  templateUrl: './company.page.html',
  styleUrls: ['./company.page.scss'],
})
export class CompanyPage implements OnInit {

  constructor(
  ) {
  }

  async ngOnInit() {
  }
}
