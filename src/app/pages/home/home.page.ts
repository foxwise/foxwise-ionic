import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'm-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit{
  
  constructor(
    private app: AppComponent,
    private platform: Platform
    ){}
  
  async ngOnInit() {
    console.log("Iniciou o Home!");
    // await this.platform.ready()
    // this.app.setUser();
  
  }
}
