import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomComponentsModule } from 'src/app/custom-components/custom-components.module';
import { HomePage } from './home.page';


@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    CustomComponentsModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
