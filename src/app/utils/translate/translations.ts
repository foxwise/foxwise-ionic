
// Importando os arquivos de linguagens
import { LANG_PT_NAME, LANG_PT_TRANS } from './lang-pt';
import { LANG_EN_NAME, LANG_EN_TRANS } from './lang-en';
import { LANG_ES_NAME, LANG_ES_TRANS } from './lang-es';
//Provider
import { InjectionToken } from '@angular/core';

// Linguagens
export const TRANSLATIONS = new InjectionToken('translations');

//Criando o dicionario.
export const dictionary = {
        "pt": LANG_PT_TRANS,
        "en": LANG_EN_TRANS,
        "es": LANG_ES_TRANS
};

// providers
export const TRANSLATION_PROVIDERS = [
	{ provide: TRANSLATIONS, useValue: dictionary },
];