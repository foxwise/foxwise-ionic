import { Branch } from './Branch';
import { Action } from './Action';
import { User } from './user';

export class Profile {

    id?: number;

    name?: string;

    branch?: Branch;

    users?: User[];

    actions?: Action[];
}
