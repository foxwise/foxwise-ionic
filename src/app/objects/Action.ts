import { Profile } from "./Profile";
import { Method } from '../enums/Method';

export interface Action {

    id?: number;

    menuLabel?: string;

    router?: string;

    url?: string;

    name?: string;
    
    group?: string;
    
    icon?: string;

    isAdmin?: boolean;
   
    method?: Method;

    profiles?: Profile[];
}
