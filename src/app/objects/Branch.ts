import { Company } from "./Company";
import { Profile } from "./Profile";
import { Status } from '../enums/Status';

export class Branch {

    id?: number;

    cnpj?: string;

    fantasy?: string;
    
    status?: Status;
    
    colorOne?: string;
    
    colorTwo?: string;
    
    colorThree?: string;

    company?: Company;

    profiles?: Profile[];

}
