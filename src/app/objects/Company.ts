import { Branch } from "./Branch";
import { Status } from '../enums/Status';

export class Company {

    id?: number;

    cnpj?: string;

    fantasy?: string;
    
    status?: Status;

    branchs?: Branch[];
}
