import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable, from } from 'rxjs';
import { UtilService } from '../services/util.service';
import { SecretToken, Routers } from '../utils/values';
import { mergeMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(
    private navController: NavController,
    private utilService: UtilService
  ){}

  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,

  ): Promise<boolean> {
    let token  = await this.utilService.getStorage(SecretToken.TOKEN);
    if (!token || token == "undefined") {
      // this.navController.navigateRoot(Routers.LOGIN);
      return true;
    }else{
      return true;
    }
  }
}
