import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from 'src/app/utils/translate/translate.service';

@Component({
  selector: 'm-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {

  @Input() icon:string;
  @Input() size:string;
  @Input() type:string;
  @Input() color:string = "primary";
  @Input() label:string;
  @Input() round:string;
  @Input() expand:string;
  @Input() disabled:boolean;
  @Input() uppercase:boolean;
  @Input() lowercase:boolean;
  @Input() iconposition:string = "left";  

  constructor(private translate: TranslateService) { }
  ngOnInit() {
    this.setValueLanguage();
    this.translate.onLangChanged.subscribe(() => {
      this.setValueLanguage();
    })
  }

  public async setValueLanguage(){
    if(this.label){
      this.label = this.translate.getValue(this.label);
    }
  }
}
