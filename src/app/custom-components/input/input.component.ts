import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ngIfFadeIn, ngIfSlideInTop } from 'src/app/utils/animates.customs';
import { TranslateService } from 'src/app/utils/translate/translate.service';

@Component({
  selector: 'm-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  animations: [
    ngIfSlideInTop,
    ngIfFadeIn, 
  ]
})
export class InputComponent implements OnInit {

  @Input() placeholder: string;
  @Input() label: string;
  @Input() icon: string;
  @Input() maxlength: number;
  @Input() debounce: number;
  @Input() type: string = "text";
  @Input() color: string = "primary";
  @Input() setMask: string;
  @Input() inputmode: string = "text";
  @Input() ctrl: FormControl = new FormControl();

  _mask: string;
  _placeholder: string;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() {
    
    this.setValueLanguage();
    this.translate.onLangChanged.subscribe(lang => {
      this.setValueLanguage();
    })

  }

  public async setValueLanguage(){
    if(this.setMask){
      this._mask = this.translate.getValue(`MASK_${this.setMask.toUpperCase()}`);
    }
    if(this.placeholder){
      this._placeholder = this.translate.getValue(this.placeholder);
    }
  }

}
