import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ngIfFadeIn, ngIfSlideInTop } from 'src/app/utils/animates.customs';

@Component({
  selector: 'm-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  animations: [ngIfSlideInTop, ngIfFadeIn]
})
export class CheckboxComponent implements OnInit {

  @Input() color: string = "primary";
  @Input() disabled: boolean;
  @Input() label: string;
  @Input() ctrl: FormControl = new FormControl();

  constructor() { }

  ngOnInit() {}

}
