import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';
import { IonicModule } from '@ionic/angular';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';
import { TranslateModule } from '../utils/translate/translate.module';
import { ButtonComponent } from './button/button.component';
import { CardComponent } from './card/card.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { DateTimeComponent } from './date-time/date-time.component';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { FloatButtonComponent } from './float-button/float-button.component';
import { HeaderSearchComponent } from './header-search/header-search.component';
import { InputComponent } from './input/input.component';
import { RadioComponent } from './radio/radio.component';
import { SelectComponent } from './select/select.component';
import { ColumnComponent } from './table/column/column.component';
import { TableComponent } from './table/table.component';
import { TextAreaComponent } from './text-area/text-area.component';
import { ToggleComponent } from './toggle/toggle.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    //Custom
    HeaderSearchComponent,
    InputComponent,
    ButtonComponent,
    FloatButtonComponent,
    CheckboxComponent,
    TextAreaComponent,
    DateTimeComponent,
    SelectComponent,
    CardComponent,
    TableComponent,
    ColumnComponent,
    DragDropComponent,
    RadioComponent,
    CheckboxComponent,
    ToggleComponent,
    FooterComponent
  ],
  imports: [
    //External
    NgxMaskIonicModule.forRoot(),

    //Angular
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,

    //Customs
    TranslateModule,

    //Material Angular
    MatTableModule,
    DragDropModule,
    MatSortModule,
    MatPaginatorModule
  ],
  exports: [
    //External
    NgxMaskIonicModule,

    //Angular
    ReactiveFormsModule,
    IonicModule,
    FormsModule,
    FormsModule,
    CommonModule,
    
    //Customs
    TranslateModule,
    HeaderSearchComponent,
    InputComponent,
    ButtonComponent,
    FloatButtonComponent,
    CheckboxComponent,
    TextAreaComponent,
    DateTimeComponent,
    SelectComponent,
    CardComponent,
    TableComponent,
    ColumnComponent,
    DragDropComponent,
    CheckboxComponent,
    RadioComponent,
    //Material Angular
    MatTableModule,
    DragDropModule,
    FooterComponent,
    MatSortModule,
    MatPaginatorModule,
    ToggleComponent
  ]
})
export class CustomComponentsModule { }
