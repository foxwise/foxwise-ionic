import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UtilService } from 'src/app/services/util.service';
import { ngIfFadeIn, ngIfSlideInTop } from 'src/app/utils/animates.customs';
import { TranslateService } from 'src/app/utils/translate/translate.service';

@Component({
  selector: 'm-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  animations: [
    ngIfSlideInTop,
    ngIfFadeIn,
  ]
})
export class SelectComponent implements OnInit {

  @Input() ctrl: FormControl = new FormControl();
  @Input() list: Array<any> = [];
  @Input() key: string = "id";
  @Input() field: string = "description";
  @Input() label: string = "SELECT";
  @Input() placeholder: string;
  @Input() multiple: boolean = false;
  @Input() okText: string = "OK";
  @Input() cancelText: string = "CANCEL";

  @Output() onClick: EventEmitter<any> = new EventEmitter();

  _format: string;
  _placeholder: string;
  _okText: string;
  _cancelText: string;
  _selectElement: ElementRef;


  constructor(
    private translate: TranslateService,
    private elementRef: ElementRef,
    private utilService: UtilService
  ) { }

  ngOnInit() {
    this.setValueLanguage();
    this.verifyTypeArray();
    //Verificando se o idioma foi alterado.
    this.translate.onLangChanged.subscribe(lang => {
      this.setValueLanguage();
    });

  }

  public async setValueLanguage() {
    if (this.placeholder) {
      this._placeholder = this.translate.getValue(this.placeholder);
    }
    if (this.okText) {
      this._okText = this.translate.getValue(this.okText);
    }
    if (this.cancelText) {
      this._cancelText = this.translate.getValue(this.cancelText);
    }
  }

  async change(event: any) {
    console.log(this.ctrl.value);
  }

  async verifyTypeArray(){
    if(this.ctrl.value instanceof Array){
      this.ctrl.setValue(this.ctrl.value.map(s => s[this.key]));
    }
  }
}
