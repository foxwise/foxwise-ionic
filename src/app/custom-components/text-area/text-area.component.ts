import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { ngIfSlideInTop, ngIfFadeIn } from 'src/app/utils/animates.custons';

@Component({
  selector: 'm-text-area',
  templateUrl: './text-area.component.html',
  styleUrls: ['./text-area.component.scss'],
  animations: [ngIfSlideInTop, ngIfFadeIn]
})
export class TextAreaComponent implements OnInit {

  @Input() disabled: boolean;
  @Input() readonly: boolean;
  @Input() maxlength: Int16Array;
  @Input() rows: Int16Array;
  @Input() cols: Int16Array;
  @Input() value: string;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() ctrl: FormControl = new FormControl();
  @Input() uppercase:boolean;
  @Input() lowercase:boolean;
  @Input() capitalizecase:boolean;
  
  _placeholder: string;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.setValueLanguage();
    this.translate.onLangChanged.subscribe(lang => {
      this.setValueLanguage();
    })
  }

  public async setValueLanguage(){
    if(this.placeholder){
      this._placeholder = this.translate.getValue(this.placeholder);
    }
  }
}
