import { Component, EventEmitter, Input, OnInit, Output, ElementRef, AfterViewChecked } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ngIfSlideInTop, ngIfSlideOutTop, ngIfScaleIn, ngIfScaleOut, ngIfSlideOutLeft, ngIfSlideInRight, ngIfFadeOut, ngIfFadeIn, ngIfSlideOutRight } from 'src/app/utils/animates.customs';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { UtilService } from 'src/app/services/util.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'm-header-search',
  templateUrl: './header-search.component.html',
  styleUrls: ['./header-search.component.scss'],
  animations: [
    ngIfSlideOutRight,
    ngIfSlideInRight,
    ngIfScaleIn,
    ngIfScaleOut
  ]
})
export class HeaderSearchComponent implements OnInit {
  // Entradas do component
  @Input() ctrl: FormControl = new FormControl();;
  @Input() showCancelButton = "always";
  @Input() animated = "animated";
  @Input() debounce = 1000;
  @Input() animate = "true";
  @Input() placeholder = this.translate.getValue(LANG_PT_TRANS.SEARCH);
  @Input() cancelButtonText = this.translate.getValue(LANG_PT_TRANS.CLOSE);
  @Output() onClose: EventEmitter<any> = new EventEmitter<any>();
  // Atributos 
  showFilters: boolean = false;
  private fieldsFilled: number;
  private ngContent: any;
  showSearch: boolean;
  @Input()
  set show(value) {
    if (this.ngContent) {
      this.countFields(this.ngContent).then(value => {
        console.log(value);
        this.fieldsFilled = value;
      });
    }
    this.showSearch = value;
  }
  constructor(
    private translate: TranslateService,
    private elementRef: ElementRef,
    private utilService: UtilService
  ) {
  }

  async ngOnInit() {
    this.cancelButtonText = this.translate.getValue("CLOSE");
    this.fieldsFilled = await this.utilService.countFieldsFilled(this.elementRef.nativeElement);
  }

  async clickCancel(ngContent) {
    this.showFilters = false;
    setTimeout(async () => {
      this.showSearch = false;
      this.onClose.emit(this.showSearch);
      this.countFields(ngContent);
    }, 100);
  }

  async setShowFilters(ngContent) {
    this.fieldsFilled = await this.countFields(ngContent);
    this.showFilters = !this.showFilters;

  }

  async countFields(ngContent) {
    if (ngContent) {
      this.ngContent = ngContent;
    }
    if (this.ngContent) {
     return  await this.utilService.countFieldsFilled(this.ngContent);
    }
  }
}
