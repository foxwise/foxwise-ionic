import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ngIfFadeIn, ngIfFadeOut, ngIfSlideInTop, ngIfSlideOutTop, ngIfScaleIn, ngIfScaleOut } from 'src/app/utils/animates.customs';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';

@Component({
  selector: 'm-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.scss'],
  animations: [ngIfFadeIn, ngIfFadeOut, ngIfSlideInTop, ngIfSlideOutTop, ngIfScaleIn, ngIfScaleOut]
})
export class DragDropComponent implements OnInit {

  @Input() ctrl: FormControl = new FormControl();
  @Input() list: Array<any> = [];
  @Input() titleSource: string = LANG_PT_TRANS.SOURCE;
  @Input() titleTarget: string = LANG_PT_TRANS.TARGET;
  @Input() multiple: boolean = false;
  @Input() key: string = "id";
  @Input() field: string = "description";
  @Input() height: string = "12em";

  _stringSelected = "selected";
  listTarget: Array<any> = [];
  constructor(
  ) { }

  ngOnInit() {
    this.verifyTypeArray();
    
  }

  updateControl(){
    let valueControl = this.listTarget.map(i => i[this.key]);
    this.cleanList();
    this.cleanListTarget();
    this.ctrl.setValue(valueControl);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
    this.updateControl();
  }

  selectItemList(item: any){
    let currentSelected: boolean = item[this._stringSelected];
    if(this.countListTargetSelected() > 0){
      this.cleanListTarget();
    }
    if(!this.multiple){
      this.cleanList(item[this.key]);
    }
    item.selected = !currentSelected;
  }

  selectItemListTarget(item: any){
    let currentSelected: boolean = item[this._stringSelected];
    if(this.countListSelected() > 0){
      this.cleanList();
    }
    if(!this.multiple){
      this.cleanListTarget(item[this.key]);
    }
    item.selected = !currentSelected;
  }

  cleanList(id: any = undefined){
    this.list.filter(i => i[this._stringSelected]).forEach(i => i[this._stringSelected] = false);
  }

  cleanListTarget(id: any = undefined){
    this.listTarget.filter(i => i[this._stringSelected]).forEach(i => i[this._stringSelected] = false);
  }

  countListSelected(): number{
    return this.list.filter( i => i[this._stringSelected]).length;
  }

  countListTargetSelected(): number{
    return this.listTarget.filter( i => i[this._stringSelected]).length;
  }

  backItem(){
    let selecteds: Array<any> = this.listTarget.filter(i => i[this._stringSelected]);
    this.listTarget = this.listTarget.filter(i => !i[this._stringSelected]);
    this.insetList1(selecteds);
  }

  forwardItem(){
    let selecteds: Array<any> = this.list.filter(i => i[this._stringSelected]);
    this.list = this.list.filter(i => !i[this._stringSelected]);
    this.insetList2(selecteds);
  }

  backAllItem(){
    let allItems = [];
    Object.assign(allItems, this.listTarget);
    this.listTarget = [];
    this.insetList1(allItems);
  }
  
  forwardAllItem(){
    let allItems = [];
    Object.assign(allItems, this.list);
    this.list = [];
    this.insetList2(allItems);

  }


  insetList1(list: Array<any> = []){
    list.forEach( i => {
      this.list.unshift(i);
    });
    this.updateControl();
  }

  insetList2(list: Array<any> = []){
    list.forEach( i => {
      this.listTarget.unshift(i);
    });
    this.updateControl();
  }

  verifyTypeArray(){
    if(this.ctrl.value instanceof Array){
      let selecteds: Array<any> = this.list.filter(i => this.ctrl.value.some(v => i[this.key] == (v[this.key] || v)));
      this.list = this.list.filter(i => !selecteds.some(s => s[this.key] == i[this.key]));
      this.insetList2(selecteds);
      // this.ctrl.setValue(this.ctrl.value.map(s => s[this.key]));
    }
  }






}
