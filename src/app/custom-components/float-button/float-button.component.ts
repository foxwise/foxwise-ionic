import { Component, OnInit, Input, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from 'src/app/utils/translate/translate.service';

@Component({
  selector: 'm-float-button',
  templateUrl: './float-button.component.html',
  styleUrls: ['./float-button.component.scss'],
})
export class FloatButtonComponent implements OnInit {

  @Input() icon: string;
  @Input() color: string = "primary";
  @Input() disabled: boolean;
  @Input() show: boolean;
  @Input() size: string;
  @Input() activated: boolean;
  @Input() label:string;

  _label: string;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this.setValueLanguage();
    this.translate.onLangChanged.subscribe(lang => {
      this.setValueLanguage();
    })
  }
  
  public async setValueLanguage(){
    if(this.label){
      this._label = this.translate.getValue(this.label);
    }
  }
}
