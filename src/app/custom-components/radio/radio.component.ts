import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ngIfSlideInTop, ngIfFadeIn } from 'src/app/utils/animates.customs';

@Component({
  selector: 'm-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
  animations: [ngIfSlideInTop, ngIfFadeIn]
})
export class RadioComponent implements OnInit {

  @Input() color: string = "primary";
  @Input() disabled: boolean;
  @Input() label: string;
  @Input() ctrl: FormControl = new FormControl();
  @Input() list: Array<any> = [];
  @Input() width: string = "100%";

  constructor() { }

  ngOnInit() {}

}
