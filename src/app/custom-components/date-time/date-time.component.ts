import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ngIfFadeIn, ngIfSlideInTop } from 'src/app/utils/animates.customs';
import { TranslateService } from 'src/app/utils/translate/translate.service';

@Component({
  selector: 'm-date-time',
  templateUrl: './date-time.component.html',
  styleUrls: ['./date-time.component.scss'],
  animations: [
    ngIfSlideInTop,
    ngIfFadeIn, 
  ]
})
export class DateTimeComponent implements OnInit {

  @Input() ctrl: FormControl = new FormControl();
  @Input() label: string;
  @Input() placeholder: string;
  @Input() min: string;
  @Input() max: string;
  @Input() format: string = "date";
  @Input() doneText: string = "DONE";
  @Input() cancelText: string = "CANCEL";

  _format: string;
  _placeholder: string;
  _doneText: string;
  _cancelText: string;
  _dayNames: string;
  _dayShortNames: string;
  _monthNames: string;
  _monthShortNames: string;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.setValueLanguage();
    //Verificando se o idioma foi alterado.
    this.translate.onLangChanged.subscribe(lang => {
       this.setValueLanguage();
    });
  }

  public async setValueLanguage(){
    if(this.format){
      this._format = this.translate.getValue(`FORMAT_${this.format.toUpperCase()}`);
    }
    if(this.placeholder){
      this._placeholder = this.translate.getValue(this.placeholder);
    }
    if(this.doneText){
      this._doneText = this.translate.getValue(this.doneText);
    }
    if(this.cancelText){
      this._cancelText = this.translate.getValue(this.cancelText);
    }
    this._dayNames = this.translate.getValue("FORMAT_DAY_NAMES");
    this._dayShortNames = this.translate.getValue("FORMAT_DAY_SHORT_NAMES");
    this._monthNames = this.translate.getValue("FORMAT_MONTH_NAMES");
    this._monthShortNames = this.translate.getValue("FORMAT_MONTH_SHORT_NAMES");
    
  }

}
