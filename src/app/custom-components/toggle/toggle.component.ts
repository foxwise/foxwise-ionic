import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from 'src/app/utils/translate/translate.service';

@Component({
  selector: 'm-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
})
export class ToggleComponent implements OnInit {

  @Input() color: string = "primary";
  @Input() disabled: boolean;
  @Input() label: string;
  @Input() position:string;
  
  _position: string;

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    this._position = this.position;
    this.setValueLanguage();
    this.translate.onLangChanged.subscribe(lang => {
      this.setValueLanguage();
    })
  }

  public async setValueLanguage(){
    if(this.label){
      this.label = this.translate.getValue(this.label);
    }
  }
}
