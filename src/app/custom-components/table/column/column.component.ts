import { Component, OnInit, Input, Optional, Host } from '@angular/core';
import { TableComponent } from '../table.component';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';

@Component({
  selector: 'm-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements OnInit {

  @Input() icon: string;
  @Input() field: string;
  @Input() label: string;
  @Input() fixed: boolean = false;//Valor que deixa a coluna da tabela fixa, se for o último campo ele ficá fixo na direita.
  @Input() sort: boolean = false;//Valor que habilita ordenação.
  @Input() isNumber: boolean = false;
  @Input() isDate: boolean = false;
  @Input() isCurrency: boolean = false;
  @Input() numberDigits: string = "1.2-2";// <Mínimo de  inteiros> . <Mínimo de frações> - <máximo de frações>
  @Input() currencySymbol: string = "";
  @Input() dateTimezone: string = "";
  @Input() dateFormat = "";


  constructor(
    @Optional() @Host() private table: TableComponent,
    private translate: TranslateService
  ) { 
    if(this.table){
      table.addColumn(this);
    }
  }
  ngOnInit(){
    if(!this.dateFormat){
      this.dateFormat = this.translate.getValue(LANG_PT_TRANS.DATE_FORMAT);
    }
    if(this.currencySymbol){
      this.currencySymbol = this.translate.getValue(this.currencySymbol);
    }
  }

}
