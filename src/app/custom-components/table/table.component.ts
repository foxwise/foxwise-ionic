import { Component, OnInit, AfterViewInit, AfterContentInit, Input, ElementRef, HostListener, ViewChild, Output, EventEmitter } from '@angular/core';
import { ColumnComponent } from './column/column.component';
import { formatNumber, formatDate, formatCurrency } from '@angular/common';
import { TranslateService } from 'src/app/utils/translate/translate.service';
import { LANG_PT_TRANS } from 'src/app/utils/translate/lang-pt';
import { UtilService } from 'src/app/services/util.service';
import { MatSort, MatPaginator, PageEvent } from '@angular/material';
import { DAOService } from 'src/app/services/dao.service';
import { merge } from 'rxjs';
import { take } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'm-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit{
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.verifyMobileTablet();
  }

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  @Input() list: Array<any>;
  @Input() data: any = {limit: 10, page: 0, qtdTotal: 0};
  @Input() striped: boolean = false;
  @Input() multiple: boolean = false;
  @Input() selected: boolean = false;
  @Input() height: string;

  @Output() onChange: EventEmitter<any> = new EventEmitter();
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  displayedColumns = [];

  listColumns: Array<ColumnComponent> = [];
  private locale = this.translate.getValue(LANG_PT_TRANS.LOCALE);
  private _isScrollTable = false
  private pageSizeOptions = [5, 10, 15, 20, 30, 40, 50, 100];
  private _selectedString = "selected";
  private listSelecteds = [];

  constructor(
    private translate: TranslateService,
    private utilService: UtilService,
    private elementRef: ElementRef
  ) {
  }

  dao: DAOService<any>;
  ngAfterViewInit(){
    setTimeout(() => {
      this.verifyMobileTablet();
    }, 500);
    if(this.data && this.data["page"] && this.data["limit"]){
      merge(this.sort.sortChange, this.paginator.page).subscribe(change => {
        let tableInfo = {};
        tableInfo[this.sort.direction] = this.sort.active;
        tableInfo["page"] = (this.paginator.pageIndex + 1);
        tableInfo["limit"] = this.paginator.pageSize;
        console.log(tableInfo);
        
        this.onChange.emit(tableInfo);
      });
    }
    // merge(this.paginator.)
    // this.dao.all()
  }

  ngOnInit() {
  }


  ngAfterContentInit() {
    this.createDisplayedColumns();
    
  }

  private createDisplayedColumns() {
    if (this.listColumns && this.listColumns.length > 0) {
      this.displayedColumns = this.listColumns.map(c => c.field);
    }
  }
  public addColumn(column: ColumnComponent) {
    this.listColumns.push(column);
  }

  getValueField(value: object, column: ColumnComponent) {
    if (value[column.field] || (!column.field && value)) {
      if (column.isNumber) {
        return formatNumber(value[column.field], this.locale, column.numberDigits)
      } else if (column.isDate) {
        return formatDate(value[column.field], column.dateFormat, this.locale);
      } else if (column.isCurrency) {
        return formatCurrency(value[column.field], this.locale, column.currencySymbol, column.numberDigits);
      } else if (column.field) {
        return this.utilService.getValueObjectField(value, column.field);
      } else {
        return value;
      }
    }
  }


  verifyMobileTablet() {
    if (this.elementRef && this.elementRef.nativeElement) {
      let linxTableContainer = this.elementRef.nativeElement.querySelector(".m-table-container");
      let linxTable = this.elementRef.nativeElement.querySelector(".m-table");
      if (linxTableContainer && linxTableContainer) {
        if (linxTable.clientWidth > linxTableContainer.clientWidth) {
          this._isScrollTable = true;
        } else {
          this._isScrollTable = false;
        }
      }
    }
  }

  private existHeaders(): boolean {
    return this.listColumns.some(c => !!c.field);
  }

  clickRow(item: any){
    let selected = !item[this._selectedString];
    if(!this.multiple){
      let list = this.data && this.data.list || this.list;
      this.list.forEach( i => i[this._selectedString] = false);
      
    }
    item[this._selectedString] = selected;
    
    // item[this._selectedString] = item[this._selectedString] ? true : false;
    let listItems: Array<any> = Object.assign([], this.data && this.data.list || this.list);
    this.listSelecteds = listItems.filter( i => i[this._selectedString]);
    this.onClick.emit(this.listSelecteds);
  }
}