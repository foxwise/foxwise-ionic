import { Injectable } from '@angular/core';
import { DAOService } from './dao.service';
import { Company } from '../objects/Company';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { delay, tap, debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CompanyService extends DAOService<Company>{
  constructor(
    protected http: HttpClient
  ) { 
    super(http, "/api/v1/company");
  }
}
