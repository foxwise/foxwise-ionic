import { DOCUMENT } from "@angular/common";
import { HttpErrorResponse, HttpClient } from "@angular/common/http";
import { Inject, Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn, FormControl, AsyncValidatorFn } from '@angular/forms';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import * as Color from "color";
import * as cryptojs from "crypto-js";
import { TranslateService } from '../utils/translate/translate.service';
import { SecretToken } from '../utils/values';
import { ObjectMSG } from '../enums/ObjectMSG';
import { TypeMSG } from '../enums/TypeMSG';
import Validation from '../utils/Validation';
import { Storage } from '@ionic/storage';
import { async } from 'q';
import { delay, map, catchError, debounceTime, distinctUntilChanged, subscribeOn } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: "root"
})
export class UtilService {

    private listLoadings: Array<HTMLIonLoadingElement> = new Array();
    private fieldsFilled: number = 0;


    constructor(
        private translate: TranslateService,
        private alertController: AlertController,
        private toastController: ToastController,
        private loadingController: LoadingController,
        private modalController: ModalController,
        @Inject(DOCUMENT) private document: Document,
        private storage: Storage,
        private http: HttpClient
    ) {
    }

    public setColorTheme(primary: string, secundary?: string, tertiary?: string) {
        let colorTheme = this.cssGenerator(primary, secundary, tertiary);
        this.setGlobalCss(colorTheme);
    }

    private setGlobalCss(css: string) {
        this.document.documentElement.style.cssText = css;
    }

    private getContrast(color, ratio = 1) {
        let colorC = Color(color);
        return colorC.isDark() ? colorC.mix(Color("white"), 1) : colorC.blacken(ratio);
    }

    private cssGenerator(primaryColor: string, secundaryColor?: string, tertiaryColor?: string) {
        let ratioTint = 0.1;
        let ratioShade = 0.1;
        let ratioSecondary = 0.7;
        let ratioTertiary = 0.2;
        let primary = Color(primaryColor);
        let secondary = secundaryColor ? Color(secundaryColor) : primary.lighten(ratioSecondary);
        let tertiary = tertiaryColor ? Color(tertiaryColor) : primary.darken(ratioTertiary);
        let rgbPrimary = primary.lighten(ratioTint).rgb().array();
        let rgbSecondary = secondary.lighten(ratioTint).rgb().array();
        let rgbTertiary = tertiary.lighten(ratioTint).rgb().array();
        return `
            --ion-color-primary: ${primary};
            --ion-color-primary-rgb: ${rgbPrimary[0]}, ${rgbPrimary[1]}, ${rgbPrimary[2]};
            --ion-color-primary-contrast: ${this.getContrast(primary)};
            --ion-color-primary-contrast-rgb: ${this.getContrast(primary).rgb().array()[0]}, ${this.getContrast(primary).rgb().array()[1]}, ${this.getContrast(primary).rgb().array()[2]};
            --ion-color-primary-shade: ${primary.darken(ratioShade)};
            --ion-color-primary-tint: ${primary.lighten(ratioTint)};
            --ion-color-secondary: ${secondary};
            --ion-color-secondary-rgb: ${rgbSecondary[0]}, ${rgbSecondary[1]}, ${rgbSecondary[2]};
            --ion-color-secondary-contrast: ${this.getContrast(secondary)};
            --ion-color-secondary-contrast-rgb: ${this.getContrast(secondary).rgb().array()[0]}, ${this.getContrast(secondary).rgb().array()[1]}, ${this.getContrast(secondary).rgb().array()[2]};
            --ion-color-secondary-shade: ${secondary.darken(ratioShade)};
            --ion-color-secondary-tint: ${secondary.lighten(ratioTint)};
            --ion-color-tertiary: ${tertiary};
            --ion-color-tertiary-rgb: ${rgbTertiary[0]}, ${rgbTertiary[1]}, ${rgbTertiary[2]};
            --ion-color-tertiary-contrast: ${this.getContrast(tertiary)};
            --ion-color-tertiary-contrast-rgb: ${this.getContrast(tertiary).rgb().array()[0]}, ${this.getContrast(tertiary).rgb().array()[1]}, ${this.getContrast(tertiary).rgb().array()[2]};
            --ion-color-tertiary-shade: ${tertiary.darken(ratioShade)};
            --ion-color-tertiary-tint: ${tertiary.lighten(ratioTint)};
            --ion-item-background: #fff;
            `;
            // --ion-background-color: linear-gradient(142deg, ${primary.desaturate(0.8).mix(Color("white"), 0.7)} 10%,  ${primary.desaturate(0.8).fade(0.4)} 90%);
    }

    public hMacSha512(text: string): string {
        try {
            let hash = cryptojs.HmacSHA512(text, SecretToken.SECRET_PUBLIC);
            return hash.toString();
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public encrypt(text: string): string {
        try {
            let ciphertext = cryptojs.AES.encrypt(JSON.stringify(text), SecretToken.SECRET_PUBLIC).toString();
            return ciphertext;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + text);
            return "";
        }
    }

    public decrypt(ciphertext: any): string {
        try {
            let bytes = cryptojs.AES.decrypt(ciphertext.toString(), SecretToken.SECRET_PUBLIC);
            var decryptedData = JSON.parse(bytes.toString(cryptojs.enc.Utf8));
            return decryptedData;
        } catch (error) {
            console.log("Error ao criar hMacSha512: " + ciphertext);
            return "";
        }
    }

    public getOnlyNumbers(value) {
        if (value) {
            try {
                return value.replace(/[^0-9]+/g, "");
            } catch (e) { console.log(e); }
        }
        return null;
    };

    public getMsgStatusError(status) {
        let msg: string = this.translate.getValue("statusDefault");
        switch (status) {
            case 0: msg = this.translate.getValue("status0"); break;
            case 302: msg = this.translate.getValue("status302"); break;
            case 304: msg = this.translate.getValue("status304"); break;
            case 400: msg = this.translate.getValue("status400"); break;
            case 401: msg = this.translate.getValue("status401"); break;
            case 403: msg = this.translate.getValue("status403"); break;
            case 404: msg = this.translate.getValue("status404"); break;
            case 405: msg = this.translate.getValue("status405"); break;
            case 410: msg = this.translate.getValue("status410"); break;
            case 500: msg = this.translate.getValue("status500"); break;
            case 502: msg = this.translate.getValue("status302"); break;
            case 503: msg = this.translate.getValue("status302"); break;
            default: break;
        }
        return msg;

    };

    public getItemList = (lista, value, campo) => {
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    item = lista[index];
                    break;
                }
            }
        }
        return item;
    };

    public getCountDuplicateList = (lista, value, campo) => {
        let count = 0;
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    count++;
                }
            }
        }
        return count;
    };

    public removeItemList = (lista, value, campo) => {
        if (!campo) { campo = "id" };
        var item = null;
        if (value && lista) {
            for (var index = 0; index < lista.length; index++) {
                if (lista[index][campo] == value) {
                    lista.splice(index, 1);
                    break;
                }
            }
        }
    };

    public filterItemList = (lista, value: string, campo: string) => {
        if (!campo) { campo = "id" };
        if (!value) return lista;
        return lista.filter(item => {
            return item[campo].toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) != -1;
        });
    };

    public changeItemList(oldList: Array<any>, newList: Array<any>, item: any) {
        if (oldList && newList) {
            let index = oldList.indexOf(item);
            if (index > -1) {
                oldList.splice(index, 1);
            }
            newList.push(item);
        }
    }

    public getValueObjectField(obj: object, field: string) {
        if (field.includes(".")) {
            let fields = field.split(".");
            fields.forEach((fieldSplit) => {
                obj = obj[fieldSplit];
            });
            return obj;
        }
        return obj[field] || "";
    }

    public validatorFunc(func: any, msgError: string, quantity?: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: string } | null => {
            if (control && !func(control.value) && (!quantity || (quantity && control.value.length >= quantity))) {
                msgError = this.translate.getValue(msgError);
                return { 'msgError': msgError };
            }
            return null;
        };
    }

    public validatorAsyncAlreadyExist(method: string, url: string, msgError: string, delayAsync?: number) {
        return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
            msgError = this.translate.getValue(msgError);
            if (!delayAsync) { delayAsync = 2000 }
            return this.http[method](`${url}/${control.value}`).pipe(
                delay(5000),
                map(data => data[ObjectMSG.OBJ] ? { "msgError": msgError } : null),
                catchError(error => {
                    if (error instanceof HttpErrorResponse && error[ObjectMSG.STATUS] == 404) {
                        return of(null);
                    } else if (error instanceof HttpErrorResponse) {
                        msgError = this.translate.getValue(`status${error[ObjectMSG.STATUS]}`);
                        return of({ "msgError": msgError });
                    }
                })
            );
        };
    }

    public validatorEquals(controlConfirm: AbstractControl, msgError: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: string } | null => {
            if (controlConfirm && control && controlConfirm.value && control.value) {
                if (controlConfirm.value != control.value) {
                    msgError = this.translate.getValue(msgError);
                    let error = { 'msgError': msgError };
                    controlConfirm.setErrors(error);
                    return error;
                } else {
                    controlConfirm.setErrors(null);
                }
            }
            return null;
        };
    }



    public getFilterPagination(filter) {
        let newFilter: string = "?";
        let page = (filter.page) ? "page=" + filter.page : "&page=1";
        let limit = (filter.limit) ? "&limit=" + filter.limit : "&limit=10";
        let asc = (filter.asc) ? "&asc=" + filter.asc : "";
        let desc = (filter.desc) ? "&desc=" + filter.desc : "";
        newFilter += page + limit + asc + desc;
        return newFilter;
    }



    public async alert(title?: string, msg?: string, subTitle?: string, buttons?: Array<any>, inputs?: Array<any>): Promise<HTMLIonAlertElement> {
        let alert: HTMLIonAlertElement = null;
        let options: any = { "backdropDismiss": false };
        if (title) { options["header"] = title }
        if (msg) { options["message"] = msg }
        if (subTitle) { options["subHeader"] = subTitle; }
        if (!buttons || buttons.length <= 0) {//Se não foi passado nenhum botão, deixa o padrão "OK".
            buttons = [{
                text: 'OK', cssClass: 'primary', "handler": () => {
                    if (alert) {
                        alert.dismiss();
                    }
                }
            }];
        }
        options["buttons"] = buttons;
        if (inputs && inputs.length > 0) { options["inputs"] = inputs; }
        alert = await this.alertController.create(options);
        await alert.present();
        return alert;
    }

    public async confirm(title: string, msg: string, onConfirm: any, onCancel: any, textConfirm: string, textCancel: string): Promise<HTMLIonAlertElement> {
        let btnConfirm = {};
        let btnCancel = {};
        btnConfirm["text"] = textConfirm ? textConfirm : this.translate.getValue("OK");
        if (onConfirm) { btnConfirm["handler"] = onConfirm }
        btnCancel["text"] = textCancel ? textCancel : this.translate.getValue("CANCEL");
        btnCancel["roler"] = "cancel";
        btnCancel["cssClass"] = "secondary";
        if (onCancel) { btnCancel["handler"] = onCancel }
        let buttons = [btnCancel, btnConfirm];
        return await this.alert(title, msg, null, buttons, null);
    }

    async toask(msg: string, position?: string, duration?: number, closeButton?: boolean, textButtonClose?: string): Promise<HTMLIonToastElement> {
        let options = {};
        options["message"] = msg;
        options["position"] = position ? position : "bottom";
        if (closeButton) { options["showCloseButton"] = true }
        if (textButtonClose) { options["closeButtonText"] = textButtonClose }
        options["duration"] = duration ? duration : 4000;
        let toast: HTMLIonToastElement = await this.toastController.create(options);
        await toast.present();
        return toast;
    }

    async loading(msg?: string, duration?: number, spinner?: string) {
        let options = {};
        options["message"] = msg ? msg : this.translate.getValue("WAIT_MODAL");
        options["duration"] = duration ? duration : 0;
        if (spinner) { options["spinner"] = spinner }
        let loading = await this.loadingController.create(options);
        this.listLoadings.push(loading);
        await loading.present();
    }

    async dismissLoading() {
        if (this.listLoadings && this.listLoadings.length > 0) {
            this.listLoadings.forEach(async loading => {
                await loading.dismiss();
            })
        }
    }

    async modal(component: any, objTranfe: any): Promise<HTMLIonModalElement> {
        const modal = await this.modalController.create({
            component: component,
            componentProps: objTranfe,
            backdropDismiss: false,
        });
        await modal.present()
        return modal;
    }

    /**
     * Função que trata as mensagem do servidor por tipo. Se for error dispara um alert, as demais toask.
     * @param data Dados do retorno do servidor.
     * @param changes Lista de objetos que mudam as mensagens. Exemplo: [{'old': '<Mensagem antiga>',  'new':  '<Nova Mensgagem'}]
     */
    async showMesseges(data: any, changes?: Array<any>): Promise<string> {
        let msgErros = "";
        if (data) {
            if (data instanceof HttpErrorResponse) {
                data = data[ObjectMSG.ERROR];
            }
            let messages: Array<any> = data[ObjectMSG.MESSAGES];
            if (messages) {
                if (changes && changes.length > 0) {//Verificando se vai alterar alguma mensagem
                    changes.forEach(change => {
                        let msgChange = messages.find((m) => m.msg == change.old);
                        if (msgChange) msgChange.msg = change.new;
                    })
                }
                messages.forEach(async message => {
                    if (message.type && message.type == TypeMSG.DANGER) {
                        if (message.msg) {
                            msgErros += this.translate.getValue(message.msg) + "\n";
                        }
                    } else if (message.msg) {
                        let toask = await this.toask(this.translate.getValue(message.msg), "bottom");
                        let dismiss = await toask.onDidDismiss();
                    }
                });
            } else if (data instanceof HttpErrorResponse) {
                let status = data[ObjectMSG.STATUS];
                if (status >= 0) {
                    console.log(`Status: ${status}`);
                    msgErros += this.translate.getValue("status" + status)
                }
            }
            await this.dismissLoading();
            if (msgErros != null && msgErros.length > 0) {
                await this.alert(null, msgErros);
                console.log('Confirm Cancel');
            }
        }
        return msgErros;
    }

    createFiltersList(list: any): string {
        let filters = "?";
        for (var key in list) {
            filters += key + "=" + list[key] + "%";
        }
        return filters;
    }

    concatObjects(sourceObject: any, destinyObject: any): any {
        for (let key in sourceObject) {
            destinyObject[key] = sourceObject[key];
        }
        return destinyObject;
    }

    public isPlatformMobile(): boolean {
        if (navigator.userAgent.match(/Android/i)
            || navigator.userAgent.match(/webOS/i)
            || navigator.userAgent.match(/iPhone/i)
            || navigator.userAgent.match(/iPad/i)
            || navigator.userAgent.match(/iPod/i)
            || navigator.userAgent.match(/BlackBerry/i)
            || navigator.userAgent.match(/Windows Phone/i)
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    public async setStorage(key: string, value: any) {
        await this.storage.set(key, value);
    }

    public async getStorage(key: string): Promise<any> {
        return await this.storage.get(key);
    }

    public async removeStorage(key: string): Promise<any> {
        return await this.storage.remove(key);
    }

    public async countFieldsFilled(element: any){
        this.fieldsFilled = 0;
        await this._countFieldsFilled(element);
        return this.fieldsFilled;
    }

    private async _countFieldsFilled(element: any) {
        if (element && element.childNodes && element.childNodes.length > 0) {
           element.childNodes.forEach(e => this._countFieldsFilled(e));
        } else if (element && element.value) {
            this.fieldsFilled++;
        }
    }
}