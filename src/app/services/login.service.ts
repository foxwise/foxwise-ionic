import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { take, delay, debounceTime } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../objects/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(user: User){
    return this.http.post<User>(`${environment.API}/api/v1/authenticate`, user).pipe(debounceTime(10000),take(1));
  }
  
  logout(){

  }

  public getAuth(){
    return this.http.get<User>(`${environment.API}/api/v1/userLogged`).pipe(take(1));
  }
}
